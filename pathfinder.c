#include <string.h>
#include <glob.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdint.h>
#include <errno.h>

#include "pathfinder.h"

extern char **environ;

struct ll_path_t {
  size_t           len;
  struct ll_path_t *next;
  char             path[];
};
typedef struct ll_path_t ll_path_t;

struct path_t {
  ll_path_t *head;
};
typedef struct path_t path_t;

static path_t
path = { 0 };

static path_t
ld_path = { 0 };

static int
glob_err_f(
    const char *path,
    int err) {

  fprintf(stderr, "glob() failed: %s\n", strerror(err));
  return 1;

}

static int
add_path(
    path_t     *path,
    const char *path_to_add) {

  int ret;
  ll_path_t *node, *prev;

  size_t len = strlen(path_to_add);

  if ( len > PATH_MAX - 1 ) {
    fprintf(stderr, "path too long: \"%s\"\n", path_to_add);
    goto err;
  }

  if ( len > ( SIZE_MAX - sizeof(ll_path_t) - 1 ) ) {
    fprintf(stderr, "size_t integer overflow\n");
    goto err;
  }

  if ( path_to_add[len - 1] != '/' ) {
    fprintf(
        stderr,
        "unexpectedly path doesn't end with '/': %s\n",
        path_to_add);
    goto ignore;
  }

  for(node = path->head, prev = 0; node; prev = node, node = node->next) {
    if ( (node->len == len) && (strncmp(path_to_add, node->path, len) == 0) ) {
      goto ignore;
    }
  }

  printf("add path %s\n", path_to_add);

  node = malloc( sizeof(ll_path_t) + len + 1 );
  if ( node == 0 ) {
    fprintf(stderr, "no mem\n");
    goto err;
  }

  node->len  = len;
  node->next = 0;

  memcpy(node->path, path_to_add, len + 1);

  if ( prev ) {
    prev->next = node;
  } else {
    path->head = node;
  }

ignore:
  ret = 0;

end:
  return ret;

err:
  ret = 1;
  goto end;

}

static int
glob_path(
    path_t     *path,
    const char *path_to_add) {

  int ret;
  glob_t gl;
  size_t i;

  printf("process %s\n", path_to_add);

  ret =
    glob(
        path_to_add,
        GLOB_NOSORT | GLOB_BRACE | GLOB_TILDE_CHECK | GLOB_ONLYDIR | GLOB_MARK,
        glob_err_f,
        &gl);

  if ( ret ) {
    fprintf(
        stderr,
        "glob() failed for \"%s\"\n",
        path_to_add);
    goto end;
  }

  for(i = 0; i < gl.gl_pathc; ++i) {
    ret = add_path(path, gl.gl_pathv[i]);
    if ( ret ) goto end;
  }

end:
  globfree(&gl);
  return ret;

}

static void
free_ll(
    ll_path_t *head) {

  ll_path_t *i = head;
  ll_path_t *n;

  while(i) {
    n = i->next;
    free(i);
    i = n;
  }

}

static void
free_path(
    path_t *path) {
  if ( path->head ) {
    free_ll(path->head);
    path->head = 0;
  }
}

static int
init_path(
    path_t     *path,
    const char *paths) {

  int ret;
  size_t i, bi;
  char buf[PATH_MAX];

  printf("prime paths with %s\n", paths);

  for(i = 0, bi = 0; paths[i]; ++i) {
    if ( paths[i] == ':' ) {
      buf[bi] = 0;

      if ( glob_path(path, buf) )
        goto err;

      bi = 0;
    } else {
      buf[bi++] = paths[i];
    }
  }

  buf[bi] = 0;
  if ( glob_path(path, buf) )
    goto err;

  ret = 0;
end:
  return ret;

err:
  free_path(path);
  ret = 1;
  goto end;

}

int
init_paths() {

  int ret;

  printf("init library search paths\n");
  if ( init_path(&ld_path, "/lib:/usr/lib") )
    goto err;

  printf("init binary search paths\n");
  // prime path finder with PATH environment variable
  for(int i = 0; environ[i]; ++i) {
    if ( strncmp("PATH=", environ[i], sizeof("PATH")) == 0 ) {
      // sizeof("PATH") = sizeof("PATH=") - 1 (trailing '\0')
      if ( init_path(&path, &(environ[i][sizeof("PATH")])) )
        goto err;
      goto ok;
    }
  }

  if ( init_path(&ld_path, "/bin:/sbin:/usr/bin:/usr/sbin") )
    goto err;

ok:
  ret = 0;
end:
  return ret;

err:
  ret = 1;
  goto end;

}

void
free_paths() {

  free_path(&path);
  free_path(&ld_path);

}

static int
exists(
    const char *path) {

  int ret = 0, c;
  struct stat st;

  printf("check if exists %s\n", path);

  c = stat(path, &st);

  if ( c == -1 ) {

    switch(errno) {
      default:
        fprintf(
            stderr,
            "stat(\"%s\") failed: %m\n",
            path);
      case EACCES:
      case ENOENT:
      case ENOTDIR:
        ;
    }

    goto end;

  }

  if ( ! S_ISREG(st.st_mode) ) {
    fprintf(
        stderr,
        "%s not regular file, ignore\n",
        path);
    goto end;
  }

  ret = 1;

end:
  return ret;

}

static const char *
find_in(
    path_t     *path,
    const char *item) {

  static char buf[PATH_MAX];
  size_t len = strlen(item);
  char *ret;

  ll_path_t *i;

  for(i = path->head; i; i = i->next) {
    if ( len > (PATH_MAX - i->len) ) {
      fprintf(stderr, "can't check for path %s because path too long\n", i->path);
      continue;
    }

    memcpy(buf, i->path, i->len);
    memcpy(&(buf[i->len]), item, len + 1);

    if ( exists(buf) ) {
      ret = buf;
      goto end;
    }
  }

  fprintf(
      stderr,
      "not found: %s\n",
      item);

  ret = 0;

end:
  return ret;

}

const char *
find_in_path(
    const char *what) {

  printf("look for binary %s\n", what);
  return find_in(&path, what);

}

const char *
find_in_ld_path(
    const char *what) {

  printf("look for library %s\n", what);
  return find_in(&ld_path, what);

}

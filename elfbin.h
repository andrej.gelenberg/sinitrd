#ifndef ELF_H
#define ELF_H

#include <elf.h>

struct strtable_t {
  const char *start;
  size_t      len;
};
typedef struct strtable_t strtable_t;

struct elfbin_t {
  void        *data;
  size_t      len;
  Elf64_Ehdr  *header;
  Elf64_Shdr  *section;
  size_t      n_sections;
  strtable_t  section_names;
};
typedef struct elfbin_t elfbin_t;

struct dynlib_i {
  Elf64_Dyn  *dyn;
  size_t     dyn_len;
  strtable_t dynstr;
  size_t     position;
  size_t     length;
};
typedef struct dynlib_i dynlib_i;

int
init_dynlib_iterator(
    elfbin_t *bin,
    dynlib_i *itr);

int
init_elfbin(
    void     *data,
    size_t   len,
    elfbin_t *bin);

Elf64_Shdr *
elfbin_get_section_header(
    elfbin_t *bin,
    size_t idx);

void *
elfbin_get_section_data(
    elfbin_t   *bin,
    Elf64_Shdr *hdr);

const char *
elfbin_get_section_name(
    elfbin_t   *bin,
    Elf64_Shdr *hdr);

int
elfbin_init_strtable(
    elfbin_t   *bin,
    Elf64_Shdr *hdr,
    strtable_t *tbl);

const char *
strtable_get(
    strtable_t *tbl,
    size_t     idx);

const char *
dynlib_next(
    dynlib_i *itr);

#endif // ELF_H

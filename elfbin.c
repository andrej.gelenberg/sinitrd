#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "elfbin.h"

static int
check_elf_header(
    elfbin_t *bin) {

  int ret;

#define expect_ident(idx, val) \
  if ( bin->header->e_ident[idx] != val ) { \
    fprintf( \
        stderr, \
        "unsupported elf binary: (%d) %d != %d\n", \
        idx, bin->header->e_ident[idx], val); \
    goto err; \
  }

  expect_ident(EI_CLASS,   ELFCLASS64);
  expect_ident(EI_DATA,    ELFDATA2LSB);
  expect_ident(EI_VERSION, EV_CURRENT);

#undef expect_ident

  if ( bin->header->e_version != EV_CURRENT ) {
    fprintf(
        stderr,
        "invalid elf version %d\n",
        bin->header->e_version);
    goto err;
  }

  if ( (bin->header->e_type != ET_EXEC) && (bin->header->e_type != ET_DYN) ) {
    fprintf(
        stderr,
        "unsupported elf file type: %d\n",
        bin->header->e_type);
    goto err;
  }

  if ( bin->header->e_machine != EM_X86_64 ) {
    fprintf(
        stderr,
        "binary or library detected, but unsupported architecture %d\n",
        bin->header->e_machine);
    goto err;
  }

  if ( bin->header->e_shoff == 0 ) {
    fprintf(
        stderr,
        "no sections in the elf file\n");
    goto err;
  }

  if ( bin->header->e_shentsize != sizeof(Elf64_Shdr) ) {
    fprintf(
        stderr,
        "unexpected section header size %d (!= %zd)\n",
        bin->header->e_shentsize,
        sizeof(Elf64_Shdr));
    goto err;
  }

  if ( bin->header->e_shoff > bin->len - sizeof(Elf64_Shdr) ) {
    fprintf(
        stderr,
        "inconsistent elf file, looks too small\n");
    goto err;
  }

  ret = 0;
end:
  return ret;

err:
  ret = 1;
  goto end;

}

static int
init_sections(
    elfbin_t *bin) {

  int ret;

  bin->section = bin->data + bin->header->e_shoff;

  if ( bin->header->e_shnum ) {

    if ( bin->section[0].sh_size != 0 ) {
      fprintf(
          stderr,
          "elf spec violations (sh_size != 0)\n");
      goto err;
    }

    bin->n_sections = bin->header->e_shnum;

  } else {

    bin->n_sections = bin->section[0].sh_size;

    if ( bin->n_sections == 0 ) {
      fprintf(
          stderr,
          "no sections in the elf file\n");
      goto err;
    }

  }

  if ( bin->n_sections > ( (bin->len - bin->header->e_shoff) / sizeof(Elf64_Shdr) ) ) {
    fprintf(
        stderr,
        "number of sections is too big, probably corrupt file\n");
    goto err;
  }

  for(size_t i = 0; i < bin->n_sections; ++i) {

    Elf64_Shdr *sec_i = &(bin->section[i]);

    if ( sec_i->sh_type == SHT_NULL || sec_i->sh_type == SHT_NOBITS )
      continue;

    if ( (sec_i->sh_offset > bin->len)
         || ( sec_i->sh_size >= (bin->len - sec_i->sh_offset) ) ) {

      fprintf(
          stderr,
          "elf section data would cause buffer overflow, file might be damaged\n");
      goto err;

    }

  }

  ret = 0;
end:
  return ret;

err:
  ret = 1;
  goto end;

}

static int
init_section_names(
    elfbin_t *bin) {

  int ret;

  Elf64_Shdr *hdr = elfbin_get_section_header(bin, bin->header->e_shstrndx);
  if ( hdr == 0 )
    goto err;

  if ( elfbin_init_strtable(bin, hdr, &(bin->section_names)) )
    goto err;

  const char *name = elfbin_get_section_name(bin, hdr);
  if ( name == 0 )
    goto err;

  if ( strcmp(".shstrtab", name) ) {
    fprintf(
        stderr,
        "invalid section names section name \"%s\" != \".shstrtab\"\n",
        name);
    goto err;
  }

  ret = 0;
end:
  return ret;

err:
  bin->section_names.start = 0;
  bin->section_names.len   = 0;
  ret = 1;
  goto end;

}

void *
elfbin_get_section_data(
    elfbin_t   *bin,
    Elf64_Shdr *hdr) {

  Elf64_Shdr *ret;

  if ( hdr->sh_type != SHT_NOBITS ) {
    ret = bin->data + hdr->sh_offset;
  } else {
    fprintf(
        stderr,
        "can't get data for SHT_NOBITS section\n");
    ret = 0;
  }

  return ret;

}

Elf64_Shdr *
elfbin_get_section_header(
    elfbin_t *bin,
    size_t    idx) {

  Elf64_Shdr *ret;

  if ( idx >= bin->n_sections ) {
    fprintf(
        stderr,
        "section index is out of range (%zd >= %zd)\n",
        idx, bin->n_sections);
    goto err;
  }

  ret = &(bin->section[idx]);

end:
  return ret;

err:
  ret = 0;
  goto end;

}

int
init_elfbin(
    void     *data,
    size_t   len,
    elfbin_t *bin) {

  int ret;

  if ( len < sizeof(Elf64_Ehdr) )
    goto err; // file way to short

  if ( (*((uint32_t*)data)) !=
      (ELFMAG0 | (ELFMAG1 << 8) | (ELFMAG2 << 16) | (ELFMAG3 << 24)) ) {
    goto err; // magic doesn't match
  }

  bin->data   = data;
  bin->len    = len;
  bin->header = data;

  if ( check_elf_header(bin) )
    goto err;

  if ( init_sections(bin) )
    goto err;

  if ( init_section_names(bin) )
    goto err;

  ret = 0;
end:
  return ret;

err:
  ret = 1;
  goto end;

}

const char *
elfbin_get_section_name(
    elfbin_t   *bin,
    Elf64_Shdr *hdr) {

  return strtable_get(&(bin->section_names), hdr->sh_name);

}

int
elfbin_init_strtable(
    elfbin_t   *bin,
    Elf64_Shdr *hdr,
    strtable_t *tbl) {

  int ret;

  if ( hdr->sh_size == 0 ) {
    fprintf(
        stderr,
        "sections name table is empty\n");
    goto err;
  }

  if ( hdr->sh_type != SHT_STRTAB ) {
    fprintf(
        stderr,
        "section names section should be SHT_STRTAB (but is %d)\n",
        hdr->sh_type);
    goto err;
  }

  tbl->start = elfbin_get_section_data(bin, hdr);
  if ( tbl->start == 0 ) goto err;

  tbl->len = hdr->sh_size;

  if ( tbl->start[tbl->len - 1] ) {
    fprintf(
        stderr,
        "string table should be '\\0' terminated\n"); 
    goto err;
  }

  ret = 0;
end:
  return ret;

err:
  ret = 1;
  goto end;

}

const char *
strtable_get(
    strtable_t *tbl,
    size_t     idx) {

  const char *ret;

  if ( idx >= tbl->len ) {
    fprintf(
        stderr,
        "invalid string index\n");
    goto err;
  }

  ret = tbl->start + idx;

end:
  return ret;
  
err:
  ret = 0;
  goto end;
}

static int
init_dyn_strtable(
    elfbin_t   *bin,
    Elf64_Shdr *hdr,
    dynlib_i   *itr) {

  int ret = 0;

  const char *name = elfbin_get_section_name(bin, hdr);
  if ( name == 0 )
    goto err;

  if ( strcmp(name, ".dynstr") )
    goto end;

  if ( itr->dynstr.start ) {
    fprintf(
        stderr,
        "more than one .dynstr section found\n");
    goto err;
  }

  if ( elfbin_init_strtable(bin, hdr, &(itr->dynstr)) )
    goto err;

end:
  return ret;

err:
  ret = 1;
  goto end;

}

static int
init_dyn_section(
    elfbin_t   *bin,
    Elf64_Shdr *hdr,
    dynlib_i   *itr) {

  int ret;

  const char *name = elfbin_get_section_name(bin, hdr);
  if ( name == 0 )
    goto err;

  if ( strcmp(name, ".dynamic") ) {
    fprintf(
        stderr,
        "SHT_DYNAMIC sections have wrong name '%s' != '.dynamic'\n",
        name);
    goto err;
  }

  if ( hdr->sh_size == 0 ) {
    fprintf(
        stderr,
        ".dynamic section is empty\n");
    goto err;
  }

  if ( hdr->sh_size % sizeof(itr->dyn[0]) ) {
    fprintf(
        stderr,
        "strange size of .dynamic section\n");
    goto err;
  }

  itr->dyn_len = hdr->sh_size / sizeof(itr->dyn[0]);

  itr->dyn = elfbin_get_section_data(bin, hdr);
  if ( itr->dyn == 0 )
    goto err;

  if ( itr->dyn[itr->dyn_len - 1].d_tag != DT_NULL ) {
    fprintf(
        stderr,
        "last element in .dynamic section should have type DT_NULL\n");
    goto err;
  }

  ret = 0;
end:
  return ret;

err:
  ret = 1;
  goto end;

}

const char *
dynlib_next(
    dynlib_i *itr) {

  const char *ret = 0;
  Elf64_Dyn *d;

  while(itr->position < itr->dyn_len) {
    d = &(itr->dyn[itr->position]);

    if (d->d_tag == DT_NULL)
      break;

    if (d->d_tag == DT_NEEDED ) {
      ret = strtable_get(&(itr->dynstr), d->d_un.d_val);
      if ( ret ) {
        ++itr->position;
        break;
      }
    }

    ++itr->position;
  };

  return ret;

}

int
init_dynlib_iterator(
    elfbin_t *bin,
    dynlib_i *itr) {

  int ret;

  itr->dyn = 0;
  itr->dynstr.start = 0;
  itr->position = 0;

  for(size_t i = 0; i < bin->n_sections; ++i) {

    Elf64_Shdr *hdr = &(bin->section[i]);

    switch(hdr->sh_type) {

      case SHT_DYNAMIC:
        if ( itr->dyn != 0 ) {
          fprintf(
              stderr,
              "according to elf spec there can be only one dyn section\n");
          goto err;
        }

        if ( init_dyn_section(bin, hdr, itr) )
          goto err;

        break;

      case SHT_STRTAB:

        if ( init_dyn_strtable(bin, hdr, itr) )
          goto err;

        break;

      default:
        ;

    }

  }

  if ( itr->dyn == 0 ) {
    fprintf(
        stderr,
        "no dynlib section found\n");
    goto err;
  }

  if ( itr->dynstr.start == 0 ) {
    fprintf(
        stderr,
        "no dynlib string table found\n");
    goto err;
  }

  ret = 0;
end:
  return ret;

err:
  ret = 1;
  goto end;

}

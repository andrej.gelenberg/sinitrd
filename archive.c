#include <archive_entry.h>
#include <glob.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <limits.h>
#include <errno.h>

#include "archive.h"
#include "fid.h"
#include "pathfinder.h"
#include "elfbin.h"

static const char *
join2path(
    char       *buf,
    size_t      buf_size,
    const char *p1,
    size_t     p1_len,
    const char *p2,
    size_t     p2_len) {

  const char *ret;

  if ( (p1_len >= buf_size) ||
       (p2_len >= buf_size) ||
       (p2_len >= (buf_size - p1_len - 1)) ) {
    fprintf(
        stderr,
        "path too long\n");
    goto err;
  }

  memcpy(buf, p1, p1_len);
  memcpy(buf + p1_len, p2, p2_len);

  buf[p1_len + p2_len] = 0;

  ret = buf;
end:
  return ret;

err:
  ret = 0;
  goto end;

}

#define cjoin2path(buf, p1, p2, p2_len) join2path(buf, sizeof(buf), p1, sizeof(p1) - 1, p2, p2_len)
#define csjoin2path(buf,p1, p2) join2path(buf, sizeof(buf), p1, sizeof(p1) - 1, p2, strlen(p2))

int
archive_open(
    archive_t  *arch,
    const char *fname) {

  int ret;

  arch->archive = archive_write_new();
  if ( arch->archive == 0 ) {
    fprintf(stderr, "archive _write_new() failed\n");
    goto archive_err;
  }

  ret = archive_write_add_filter_gzip(arch->archive);
  if ( ret != ARCHIVE_OK ) {
    fprintf(
        stderr,
        "archive_write_add_filter_gzip() failed: %s\n",
        archive_error_string(arch->archive)
      );
    goto err;
  }

  ret = archive_write_set_format_cpio_newc(arch->archive);
  if ( ret != ARCHIVE_OK ) {
    fprintf(
        stderr,
        "archive_write_set_format_cipo_newc() failed: %s\n",
        archive_error_string(arch->archive)
      );
    goto err;
  }

  ret = archive_write_open_filename(arch->archive, fname);
  if ( ret != ARCHIVE_OK ) {
    fprintf(
        stderr,
        "archive_write_open_filename() failed: %s\n",
        archive_error_string(arch->archive)
      );
    goto err;
  }

  arch->entry = archive_entry_new();

  if ( arch->entry == 0 ) {
    fprintf(
        stderr,
        "archive_entry_new() failed\n");
    goto err;
  }

  ret = 0;
end:
  return ret;

archive_err:
  archive_write_free(arch->archive);
err:
  ret = 1;
  goto end;

}

int
archive_close(
    archive_t *arch) {

  int ret = 0;
  int s;

  archive_entry_free(arch->entry);

  s = archive_write_close(arch->archive);
  if ( s != ARCHIVE_OK ) {
    fprintf(
        stderr,
        "archive_write_close() failed: %s\n",
        archive_error_string(arch->archive)
      );
    ret = 1;
  }

  s = archive_write_free(arch->archive);
  if ( s != ARCHIVE_OK ) {
    fprintf(
        stderr,
        "archive_write_free() failed\n"
      );
    ret = 1;
  }

  return ret;

}

static int
glob_err_f(
    const char *path,
    int err) {

  fprintf(stderr, "glob() failed: %s\n", strerror(err));
  return 1;

}

static int
add_hlink(
    archive_t  *arch,
    const char *hlink,
    const char *to,
    mode_t     mode) {

  int ret;

  printf("add hard link %s -> %s\n", hlink, to);

  archive_entry_set_pathname(arch->entry, to);
  archive_entry_set_perm(arch->entry, mode);
  archive_entry_set_filetype(arch->entry, AE_IFREG);
  archive_entry_set_hardlink(arch->entry, hlink);

  ret = archive_write_header(arch->archive, arch->entry);
  if ( ret != ARCHIVE_OK ) {
    fprintf(
        stderr,
        "archive_write_header() for %s failed: %s\n",
        to,
        archive_error_string(arch->archive));
    goto err;
  }

  ret = 0;
end:
  archive_entry_clear(arch->entry);
  return ret;

err:
  ret = 1;
  goto end;

}

static int
add_lib(
    archive_t *arch,
    const char *lib) {

  int ret;
  static char buf[PATH_MAX];

  const char *to = csjoin2path(buf, "/lib/", lib);
  if ( to == 0 )
    goto err;

  nbst_ret_t r =
    nbst_cstr_search(
        file_repo,
        to,
        0);

  if ( r == NBST_OK ) {
    ret = 0;
    goto end;
  }

  if ( r != NBST_NOTFOUND ) {
    fprintf(
        stderr,
        "nbst_cstr_search(file_repo, \"%s\") failed: %s\n",
        to,
        nbst_error_message(r));
    goto err;
  }

  const char *src = find_in_ld_path(lib);
  if ( src == 0 )
    goto err;

  ret = archive_add_file(arch, src, to, 0400);

end:
  return ret;

err:
  ret = 1;
  goto end;

}

static int
add_depended_libs(
    archive_t *arch,
    void      *data,
    size_t    len) {

  int ret;
  const char *lib_name;
  elfbin_t bin;

  if ( init_elfbin(data, len, &bin) )
    goto skip;

  printf("check libraries dependencies\n");

  dynlib_i itr;

  if ( init_dynlib_iterator(&bin, &itr) )
    goto skip;

  while( (lib_name = dynlib_next(&itr)) ) {
    if ( add_lib(arch, lib_name) )
      goto err;
  }

  printf("finished checking library dependencies\n");

skip:
  ret = 0;
end:
  return ret;

err:
  ret = 1;
  goto end;

}

static int
add_file(
    archive_t  *arch,
    void       *data,
    size_t     len,
    const char *to,
    mode_t     mode) {

  int ret;

  printf("add file %s (%zd) mode 0%3o\n", to, len, mode);

  archive_entry_set_pathname(arch->entry, to);
  archive_entry_set_perm(    arch->entry, mode);
  archive_entry_set_filetype(arch->entry, AE_IFREG);
  archive_entry_set_size(    arch->entry, len);

  ret = archive_write_header(arch->archive, arch->entry);
  if ( ret != ARCHIVE_OK ) {
    fprintf(
        stderr,
        "archive_write_header failed: %s\n",
        archive_error_string(arch->archive));
    goto err;
  }

  size_t w = archive_write_data(arch->archive, data, len);

  if ( w != len ) {
    fprintf(
        stderr,
        "archive_write_data failed: %s (%zd != %zd)\n",
        archive_error_string(arch->archive),
        w, len);
    goto err;
  }
  archive_entry_clear(arch->entry);

  ret = add_depended_libs(arch, data, len);

end:
  return ret;

err:
  ret = 1;
  archive_entry_clear(arch->entry);
  goto end;

}

static int
check_file_duplicates(
    archive_t  *arch,
    void       *data,
    size_t     len,
    const char *to,
    mode_t     mode) {

  int ret;
  SHA256_CTX c;
  fhash md;

  ret = SHA256_Init(&c);
  if ( ret != 1 ) {
    fprintf(stderr, "SHA256_Init() failed\n");
    goto err;
  }

  ret = SHA256_Update(&c, data, len);

  if ( ret != 1 ) {
    fprintf(stderr, "SHA256_Update() failed\n");
    goto err;
  }

  ret = SHA256_Final(md.h, &c);

  if ( ret != 1 ) {
    fprintf(stderr, "SHA256_Final() failed\n");
    goto err;
  }

  const char *hlink = fhash_find(md, to);

  if ( hlink ) {

    printf("file with same hash found\n");
    ret = add_hlink(arch, hlink, to, mode);

  } else {

    ret = add_file(arch, data, len, to, mode);

  }

end:
  return ret;

err:
  ret = 1;
  goto end;

}

static int
mmap_file(
    archive_t  *arch,
    int        fd,
    size_t     len,
    const char *to,
    mode_t     mode) {

  int ret;

  void *data =
    mmap(
        0,
        len,
        PROT_READ,
        MAP_PRIVATE,
        fd,
        0);

  if ( data == MAP_FAILED ) {
    fprintf(
        stderr,
        "mmap() failed: %m\n");
    goto err;
  }

  ret = check_file_duplicates(arch, data, len, to, mode);

  if ( munmap(data, len) == -1 ) {
    fprintf(
        stderr,
        "munmap() failed: %m\n");
  }

end:
  return ret;

err:
  ret = 1;
  goto end;

}


static int
check_file_stat(
    archive_t  *arch,
    const char *from,
    const char *to,
    mode_t     mode) {

  int ret;
  struct stat st;

  int fd = open(from, O_RDONLY);
  if ( fd == -1 ) {
    fprintf(
        stderr,
        "open(\"%s\") failed: %m\n",
        from);
    goto err;
  }

  if ( fstat(fd, &st) ) {
    fprintf(stderr, "stat(\"%s\") failed: %m\n", from);
    goto close_err;
  }

  if ( ! S_ISREG(st.st_mode) ) {
    fprintf(
        stderr,
        "currently only regular files are supported, \"%s\" is not regular file",
        from);
    goto close_err;
  }

  const char *hlink = fid_find(st.st_dev, st.st_ino, to);

  if ( hlink ) {
    printf("hard linked file detected");
    ret = add_hlink(arch, hlink, to, mode);
  } else {
    ret = mmap_file(arch, fd, st.st_size, to, mode);
  }

close:
  if ( close(fd) == -1 ) {
    fprintf(
        stderr,
        "close() failed: %m\n");
  }

end:
  return ret;

close_err:
  ret = 1;
  goto close;

err:
  ret = 1;
  goto end;

}

int
archive_add_dir(
    archive_t  *arch,
    const char *path) {

  int ret;

  printf("add dir %s\n", path);

  archive_entry_set_pathname(arch->entry, path);
  archive_entry_set_size(arch->entry, 0);
  archive_entry_set_perm(arch->entry, 0500);
  archive_entry_set_filetype(arch->entry, AE_IFDIR);

  ret = archive_write_header(arch->archive, arch->entry);
  if ( ret != ARCHIVE_OK ) {
    fprintf(
        stderr,
        "archive_write_seader() for dir %s failed: %s\n",
        path,
        archive_error_string(arch->archive));
    goto err;
  }

  ret = 0;

end:
  return ret;

err:
  ret = 1;
  goto end;

}

static int
add_all_dirs(
    archive_t *arch,
    char      *buf,
    size_t    len) {

  int i;
  int ret = 0;
  nbst_ret_t c;

  c = nbst_cstr_insert(file_repo, buf);
  if ( c == NBST_EXISTS )
    goto end;

  if ( c != NBST_OK ) {
    fprintf(
        stderr,
        "nbst_cstr_insert(file_repo, \"%s\") failed: %s\n",
        buf,
        nbst_error_message(c));
    goto err;
  }

  for(i = len; i && buf[i] != '/'; --i);

  if ( i >= 1 ) {
    buf[i] = 0;
    ret = add_all_dirs(arch, buf, i);
    if ( ret ) goto end;
    buf[i] = '/';
  }

  ret = archive_add_dir(arch, buf);

end:
  return ret;

err:
  ret = 1;
  goto end;

}

static int
add_file_subdirs(
    archive_t *arch,
    const char *dir) {

  static char buf[PATH_MAX];
  int ret;

  size_t last_slash = 0;
  size_t i;

  for(i = 0; dir[i]; ++i) {
    buf[i] = dir[i];
    if ( dir[i] == '/' ) {
      last_slash = i;
    }
  }

  if ( last_slash > 1 ) {

    buf[last_slash] = 0;
    ret = add_all_dirs(arch, buf, last_slash);

  } else {

    ret = 0;

  }

  return ret;
}

int
archive_add_file(
    archive_t  *arch,
    const char *from,
    const char *to,
    mode_t     mode) {

  printf("add \"%s\" -> \"%s\" (0%o)\n", from, to, mode);

  int    ret;
  glob_t gl;

  nbst_ret_t r =
    nbst_cstr_insert(
        file_repo,
        to);

  if ( r == NBST_EXISTS ) {
    fprintf(
        stderr,
        "file collision: %s already in the archive\n",
        to);
    goto err;
  }

  if ( r != NBST_OK ) {
    fprintf(
        stderr,
        "nbst_cstr_insert() failed: %s\n",
        nbst_error_message(r));
    goto err;
  }

  ret = add_file_subdirs(arch, to);

  if ( ret )
    goto err;

  ret =
    glob(
        from,
        GLOB_NOSORT | GLOB_BRACE | GLOB_TILDE_CHECK,
        glob_err_f,
        &gl);

  if ( ret ) {
    fprintf(stderr, "glob() failed for \"%s\"\n", from);
    goto glob_err;
  }

  if ( gl.gl_pathc > 1 ) {
    fprintf(
        stderr,
        "ambigious source, choose \"%s\", others:\n",
        gl.gl_pathv[0]);
    for(int i = 1; i < gl.gl_pathc; ++i) {
      fprintf(stderr, "  %s\n", gl.gl_pathv[i]);
    }
  }

  ret =
    check_file_stat(
        arch,
        gl.gl_pathv[0],
        to,
        mode);

glob_end:
  globfree(&gl);
end:
  return ret;

glob_err:
  ret = 1;
  goto glob_end;

err:
  ret = 1;
  goto end;

}

int
archive_add_bin(
    archive_t  *arch,
    const char *bin) {

  int ret;
  size_t i, fname_i = 0;
  static char buf[PATH_MAX];

  for(i=0; bin[i]; ++i) {
    if (bin[i] == '/') {
      fname_i = i + 1;
    }
  }

  const char *to = cjoin2path(buf, "/bin/", bin + fname_i, i - fname_i);

  if ( fname_i == 0 ) {
    bin = find_in_path(bin);
    if ( bin == 0 ) goto err;
  }

  ret = archive_add_file(arch, bin, to, 0500);

end:
  return ret;

err:
  ret = 1;
  goto end;
}

#ifndef ARCHIVE_H
#define ARCHIVE_H

#include <sys/stat.h>
#include <archive.h>

struct archive_t {
  struct archive       *archive;
  struct archive_entry *entry;
};
typedef struct archive_t archive_t;

int
archive_open(
    archive_t  *arch,
    const char *fname);

int
archive_close(
    archive_t *arch);

int
archive_add_file(
    archive_t  *arch,
    const char *src,
    const char *tgt,
    mode_t     mode);

int
archive_add_bin(
    archive_t  *arch,
    const char *bin_name);

int
archive_add_lib(
    archive_t  *arch,
    const char *lib);

int
archive_add_dir(
    archive_t *arch,
    const char *path);

#endif // ARCHIVE_H

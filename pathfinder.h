#ifndef PATHFINDER_H
#define PATHFINDER_H

const char *
find_in_path(
    const char *what);

const char *
find_in_ld_path(
    const char *what);

int
init_paths();

void
free_paths();

#endif // PATHFINDER_H

#ifndef DEVID_H
#define DEVID_H

#include <sys/stat.h>
#include <nbst/nbst_lib.h>
#include <openssl/sha.h>
#include <stdint.h>

struct fid {
  dev_t device;
  ino_t inode;
};
typedef struct fid fid;

int
fid_cmp(
    fid a,
    fid b);

#define KEY_T   fid
#define VALUE_T cstr
#include <nbst/nbst_api.h>

struct fhash {
  uint8_t h[SHA256_DIGEST_LENGTH];
};
typedef struct fhash fhash;

#define KEY_T fhash
#define VALUE_T cstr
#include <nbst/nbst_api.h>

extern nbst_fid_cstr_t *
fid_repo;

extern nbst_fhash_cstr_t *
fhash_repo;

extern nbst_cstr_t *
file_repo;

const char *
fid_find(
    dev_t dev,
    ino_t ino,
    const char *to);

const char *
fhash_find(
    fhash h,
    const char *to);

#endif // DEVID_H

all: mksinit

-include *.d
-include config.mk

CFLAGS ?= -O2
LDFLAGS = -lnbst -larchive -lcrypto
CC     ?= gcc

CFLAGS += -MD

build/bin/busybox: /home/andrej/vendor/busybox/busybox
	cp '$<' '$@'

mksinit: mksinit.o archive.o fid.o pathfinder.o elfbin.o config.o

initrd.gz: mksinit
	./'$<' '$@'

.PHONY: clean
clean:
	-rm initrd.gz *.o *.d mksinit

%: %.o Makefile
	$(CC) -o '$@' $(CFLAGS) $(LDFLAGS) $(filter %.o,$^)

%.o: %.c Makefile
	$(CC) -c -o '$@' $(CFLAGS) '$<'

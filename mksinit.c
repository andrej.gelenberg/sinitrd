#include <stdio.h>
#include <nbst/nbst_lib.h>
#include <string.h>

#include "tools.h"
#include "archive.h"
#include "config.h"
#include "fid.h"
#include "pathfinder.h"

static void
usage() {
  fprintf(stderr, "usage: mkinit <output>\n");
}

int
main(
    int  argc,
    char **argv) {

  int ret;
  archive_t arch;

  if ( argc != 2 || argv[1][0] == 0 ) {
    usage();
    ret = 1;
    goto end;
  }

  ret = init_paths();
  if ( ret ) goto end;

  // init file id repo
  nbst_ret_t c = nbst_fid_cstr_new(&fid_repo, 0);
  if ( c != NBST_OK ) {
    fprintf(
        stderr,
        "can't init file id repository: %s\n",
        nbst_error_message(c));
    goto fid_err;
  }

  // init file hash repo
  c = nbst_fhash_cstr_new(&fhash_repo, 0);
  if ( c != NBST_OK ) {
    fprintf(
        stderr,
        "can't init file hash repository: %s\n",
        nbst_error_message(c));
    goto fhash_err;
  }

  // init file name repo
  c = nbst_cstr_new(&file_repo, 0);
  if ( c != NBST_OK ) {
    fprintf(
        stderr,
        "can't init file repository: %s\n",
        nbst_error_message(c));
    goto file_err;
  }

  ret = archive_open(&arch, argv[1]);
  if ( ret ) goto fhash_end;

  ret = archive_add_file(&arch, init_binary, "/init", 0500);
  if (ret) goto arch_open_end;

  // add directories
  for(size_t i = 0; i < dirs_len; ++i) {
    ret = archive_add_dir(&arch, dirs[i]);
    if (ret) goto arch_open_end;
  }

  // include executables
  for(size_t i = 0; i < include_binaries_len; ++i) {
    ret = archive_add_bin(&arch, include_binaries[i]);
    if (ret) goto arch_open_end;
  }

  // include non executables file
  for(size_t i = 0; i < include_files_len; ++i) {
    ret =
      archive_add_file(
          &arch,
          include_files[i][0],
          include_files[i][1],
          0400
        );
    if (ret) goto arch_open_end;
  }

arch_open_end:
  archive_close(&arch);

file_end:
  nbst_cstr_free(file_repo);

fhash_end:
  nbst_fhash_cstr_free(fhash_repo);

fid_end:
  nbst_fid_cstr_free(fid_repo);

pathfinder_end:
  free_paths();

end:
  return ret;

file_err:
  ret = 1;
  goto file_end;

fhash_err:
  ret = 1;
  goto fid_end;

fid_err:
  ret = 1;
  goto pathfinder_end;
}

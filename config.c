#include "config.h"

const char *
dirs[] = {
  "/etc",
  "/bin",
  "/lib",
  "/proc",
  "/dev",
  "/sys",
  "/root"
};

size_t
dirs_len =
    sizeof(dirs) / sizeof(dirs[0]);

const char *
include_binaries[] = {
  "dash",
  "bash",
  "strace",
  "busybox",
  "cryptsetup",
  "lvm",
  "findfs",
  "xfs_growfs",
  "xfs_repair",
  "xfs_scrub",
  "switch_root",
  "loadkeys",
  "true",
  "mount",
  "cat",
  "echo",
  "cut"
};

size_t
include_binaries_len =
    sizeof(include_binaries) / sizeof(include_binaries[0]);

const char *
include_files[][2] = {
  {"kmap",  "/etc/kmap"}
};

size_t
include_files_len = 
   sizeof(include_files) / sizeof(include_files[0]);

const char *
init_binary = "scripts/init";

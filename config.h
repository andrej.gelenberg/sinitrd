#ifndef CONFIG_H
#define CONFIG_H

#include "stddef.h"

extern const char *
dirs[];

extern size_t
dirs_len;

extern const char *
include_binaries[];

extern size_t
include_binaries_len;

extern const char *
include_files[][2];

extern size_t
include_files_len;

extern const char *
init_binary;

#endif // CONFIG_H

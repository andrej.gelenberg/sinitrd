#include <string.h>
#include <stdio.h>

#include "fid.h"

#define KEY_T   fid
#define VALUE_T cstr
#define KEY_CMP
#define VALUE_COPY
#define VALUE_FREE
#include <nbst/nbst_impl.h>

#define KEY_T   fhash
#define VALUE_T cstr
#define KEY_CMP
#define VALUE_COPY
#define VALUE_FREE
#include <nbst/nbst_impl.h>


nbst_fid_cstr_t *
fid_repo = 0;

nbst_fhash_cstr_t *
fhash_repo = 0;

nbst_cstr_t *
file_repo = 0;

int
nbst_fid_cstr_key_cmp(
    fid a,
    fid b) {

  return (b.device - a.device) || (b.inode - a.inode);

}

int
nbst_fhash_cstr_key_cmp(
    fhash a,
    fhash b) {

  return memcmp(a.h, b.h, sizeof(a.h));

}

static nbst_ret_t
nbst_fid_cstr_value_copy(
          cstr *tar,
    const cstr from) {

  return ((*tar = strdup(from)) == 0) ? NBST_NOMEM : NBST_OK;

}

static nbst_ret_t
nbst_fhash_cstr_value_copy(
    cstr *tar,
    const cstr from) {

  return nbst_fid_cstr_value_copy(tar, from);

}

static void
nbst_fid_cstr_value_free(
    cstr v) {
  free((void*)v);
}

static void
nbst_fhash_cstr_value_free(
    cstr v) {
  free((void*)v);
}

const char *
fid_find(
    dev_t dev,
    ino_t ino,
    const char *to) {

  fid f = { dev, ino };
  const char *ret;

  nbst_ret_t r =
    nbst_fid_cstr_search(
        fid_repo,
        f,
        0,
        &ret);

  if ( r != NBST_OK ) {
    r = nbst_fid_cstr_set(fid_repo, f, to);
    if ( r != NBST_OK ) {
      fprintf(
          stderr,
          "nbst_fid_cstr_set() failed: %s\n",
          nbst_error_message(r));
    }
    ret = 0;
  }

  return ret;

}

const char *
fhash_find(
    fhash h,
    const char *to) {

  const char *ret;

  nbst_ret_t r =
    nbst_fhash_cstr_search(
        fhash_repo,
        h,
        0,
        &ret);

  if ( r != NBST_OK ) {
    r = nbst_fhash_cstr_set(fhash_repo, h, to);
    if ( r != NBST_OK ) {
      fprintf(
          stderr,
          "nbst_fhash_cstr_set() failed: %s\n",
          nbst_error_message(r));
    }

    ret = 0;
  }

  return ret;

}
